package io.cloudrepo.examples.maven.math;

/**
 * Implement some basic math functions for use in applications.
 *
 * This class will be published as part of an example library and can be consumed as a dependency in application projects.
 */
public class Functions {


    /**
     * Add a series of integers.
     *
     * @param numbers the numbers you wish to add.
     * @return The mathematical sum (addition) of the provided numbers.
     */
    public static int add(int... numbers){

        int result = 0;
        for (int number : numbers) {
            result = number + result;
        }
        return result;
    }
}
